package com.epam.bookservice.controllertest;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.bookservice.controller.BookController;
import com.epam.bookservice.entity.Book;
import com.epam.bookservice.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest
public class BookControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	BookController bookController; 

	@MockBean
	BookService bookService;

	private List<Book> bookList = new ArrayList<>();

	@BeforeEach
	public void setup() {
		bookList.add(new Book(1L, "The DaVinci Code", "Thriller", "DanBrown"));
		bookList.add(new Book(2l, "Inferno", "Thriller", "DanBrown"));
	}

	@Test
	public void findAllBooks_successfully() throws Exception {
		when(bookService.findAllBooks()).thenReturn(bookList);
		this.mockMvc.perform(get("/books")).andExpect(status().isOk());
	}

	@Test
	public void findBook_whenValidId_successfully() throws Exception {
		when(bookService.findBook(Mockito.anyLong())).thenReturn(bookList.get(0));
		this.mockMvc.perform(get("/books/1")).andExpect(status().isOk());
	}

	@Test
	public void addBook_whenValidBook_successfully() throws Exception {
		when(bookService.addBook(Mockito.any())).thenReturn(bookList.get(0));
		this.mockMvc.perform(post("/books").contentType("application/json").content(toJsonString(bookList.get(0))))
				.andExpect(status().isCreated());
	}

	@Test
	public void updateBook_whenValidIdAndBookAreGiven() throws Exception {
		when(bookService.updateBook(1L, bookList.get(0))).thenReturn(bookList.get(0));
		this.mockMvc.perform(put("/books/1").contentType("application/json").content(toJsonString(bookList.get(0))))
				.andExpect(status().isOk());
	}

	@Test
	public void deleteBook_whenValidID_successfully() throws Exception {
		doNothing().when(bookService).deleteBook(1L);
		this.mockMvc.perform(delete("/books/1")).andExpect(status().isNoContent());
	}

	public static String toJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
