package com.epam.ecommerce.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.bookservice.entity.Book;
import com.epam.bookservice.exception.BookException;
import com.epam.bookservice.exception.BookNotFoundException;
import com.epam.bookservice.repository.BookRepository;
import com.epam.bookservice.service.BookService;

public class BookServiceTest {

	@InjectMocks
	BookService bookService;

	@Mock
	BookRepository bookRepository;

	private Book book;
 
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		book = new Book(1L, "The DaVinci Code", "Thriller", "DanBrown");
	}
 
	@Test
	public void findAllBooks_successfully() {
		List<Book> booksList = new ArrayList<>();
		booksList.add(book);
		when(bookRepository.findAll()).thenReturn(booksList);
		List<Book> actualList = bookService.findAllBooks();
		for (int i = 0; i < booksList.size(); i++) {
			assertEquals(booksList.get(i), actualList.get(i));
		}

	}

	@Test
	public void findBook_whenValidId_successfully() {
		Optional<Book> expectedBook = Optional.of(book);
		when(bookRepository.findById(book.getId())).thenReturn(expectedBook);
		Book actualBook = bookService.findBook(1L);
		assertEquals(actualBook.getTitle(), book.getTitle());
	}

	@Test
	public void addBook_whenValidBook_successfully() {
		when(bookRepository.save(Mockito.any())).thenReturn(book);
		assertEquals(bookService.addBook(book).getTitle(), book.getTitle());

	}

	@Test
	public void updateBook_whenValidIdAndBookAreGiven() {
		Optional<Book> expectedBook = Optional.of(book);
		when(bookRepository.findById(Mockito.anyLong())).thenReturn(expectedBook);
		when(bookRepository.existsById(Mockito.anyLong())).thenReturn(true);
		when(bookRepository.save(book)).thenReturn(book);
		assertEquals(bookService.updateBook(book.getId(), book).getTitle(), book.getTitle());

	}

	@Test
	public void deleteBook_whenValidID_successfully() {
		bookService.deleteBook(Mockito.anyLong());
		verify(bookRepository, times(1)).deleteById(Mockito.anyLong());
	}

	@Test
	public void updateBook_whenInValidId_throwBookException() {
		Optional<Book> expectedBook = Optional.of(book);
		when(bookRepository.findById(Mockito.anyLong())).thenReturn(expectedBook);
		Assertions.assertThrows(BookException.class, () -> {
			bookService.updateBook(2L, book);
		});

	}

	@Test
	public void findBook_whenInvaliID_throwBookNotFoundException() {
		Optional<Book> expectedBook = Optional.of(book);
		when(bookRepository.findById(book.getId())).thenReturn(expectedBook);
		Assertions.assertThrows(BookNotFoundException.class, () -> {
			bookService.findBook(2L);
		});
	}
}
