package com.epam.bookservice.exception;

public class BookException extends RuntimeException {

	public BookException(String message) {
		super(message);
	}
}
