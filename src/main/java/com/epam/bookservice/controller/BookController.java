package com.epam.bookservice.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.bookservice.entity.Book;
import com.epam.bookservice.service.BookService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("/books")
public class BookController {

	@Autowired
	BookService bookService;

	@ApiOperation(value = "List of all books available", response = ResponseEntity.class)
	@ApiResponse(code = 200, message = "Successfully retrieved all books")
	@GetMapping
	public ResponseEntity<List<Book>> findAllBooks() {
		return ResponseEntity.ok(bookService.findAllBooks());
	}

	@ApiOperation(value = "Serach a particular book", response = ResponseEntity.class)
	@ApiImplicitParam(name = "bookId", value = "The unique id of the book", required = true, dataType = "Long", paramType = "path")
	@ApiResponse(code = 200, message = "Successfully retrieved book")
	@GetMapping("/{bookId}")
	public ResponseEntity<Book> findABook(@PathVariable Long bookId) {
		return ResponseEntity.ok(bookService.findBook(bookId));
	}

	@ApiOperation(value = "Add a book", response = ResponseEntity.class)
	@ApiImplicitParam(name = "book", value = "The body of book to be added", required = true, dataType = "Book", paramType = "body")
	@ApiResponse(code = 201, message = "Successfully added a book")
	@PostMapping
	public ResponseEntity<Book> addBook(@RequestBody Book book) {
		Book savedBook = bookService.addBook(book);
		final URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedBook.getId()).toUri();
		return ResponseEntity.created(location).body(savedBook);
	}

	@ApiOperation(value = "Update a book", response = ResponseEntity.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name = "bookId", value = "The unique ID of the book", required = true, dataType = "Long", paramType = "path"),
			@ApiImplicitParam(name = "book", value = "The body of the book to be updated", required = true, dataType = "Book", paramType = "body") })
	@ApiResponse(code = 200, message = "Successfully updated the book")
	@PutMapping("/{bookId}")
	public ResponseEntity<Book> updateBook(@PathVariable Long bookId, @RequestBody Book book) {
		return ResponseEntity.ok(bookService.updateBook(bookId, book));
	}

	@ApiOperation(value = "Delete a book", response = ResponseEntity.class)
	@ApiImplicitParam(name = "bookId", value = "The unique id of the book", required = true, dataType = "Long", paramType = "path")
	@ApiResponse(code = 204, message = "Successfully deleted the book")
	@DeleteMapping("/{bookId}")
	public ResponseEntity<?> deleteBook(@PathVariable Long bookId) {
		bookService.deleteBook(bookId);
		return ResponseEntity.noContent().build();
	}
}
