package com.epam.bookservice.service;

import java.util.List;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.bookservice.entity.Book;
import com.epam.bookservice.exception.BookException;
import com.epam.bookservice.exception.BookNotFoundException;
import com.epam.bookservice.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;

	public List<Book> findAllBooks() {
		return bookRepository.findAll();
	}

	public Book findBook(Long bookId) {
		return bookRepository.findById(bookId).orElseThrow(bookNotFound(bookId));
	}

	public Book addBook(Book book) {
		if (bookRepository.existsById(book.getId()))
			throw new BookException("Book already exists.."); 
		return bookRepository.save(book);
	}

	public Book updateBook(Long bookId, Book book) {
		if (bookId != book.getId() || !bookRepository.existsById(bookId))
			throw new BookException("Bad Request.." + bookId);
		return bookRepository.findById(bookId).map(existingBook -> bookRepository.save(book))
				.orElseThrow(bookNotFound(bookId));
	}

	private Supplier<BookNotFoundException> bookNotFound(Long bookId) {
		return () -> new BookNotFoundException("Book not Found:  " + bookId);
	}

	public void deleteBook(Long bookId) {
		bookRepository.deleteById(bookId);

	}
}
