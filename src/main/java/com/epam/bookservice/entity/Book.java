package com.epam.bookservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "The auto-generated book ID", dataType = "Long", required = true)
	private Long id;
	@ApiModelProperty(notes = "Title of the book", dataType = "String", required = true)
	private String title;
	@ApiModelProperty(notes = "Genre of the book", dataType = "String", required = true)
	private String genre;
	@ApiModelProperty(notes = "Author of the book", dataType = "String", required = true)
	private String author;

	public Book(Long id, String title, String genre, String author) {
		super();
		this.id = id;
		this.title = title;
		this.genre = genre;
		this.author = author;
	}

	public Long getId() {
		return id; 
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", title=" + title + ", genre=" + genre + ", author=" + author + "]";
	}

}
